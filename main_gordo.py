from fastapi import FastAPI #crea la app
from fastapi import HTTPException
from fastapi import status # status code con alguna definicion
from fastapi import Request
from fastapi.responses import HTMLResponse # permite respuestas en formato HTML
from fastapi.responses import JSONResponse # permite repuestas en formato Json
from fastapi.security import HTTPBearer

from pydantic import BaseModel #permite la creacion facil de modelos

from utils.jwt_manager import create_token
from utils.jwt_manager import validate_token

from config.database import d_base, engine
from routers.movie import movie_router
from routers.user import user_router
from middlewares.error_handler import ErrorHandler

# inicia la app
app =FastAPI()
app.title = "Mi aplicacion con FastApi (Joan)"
app.version = "0.0.1"
app.include_router(movie_router)
app.include_router(user_router)
#app.add_middleware(ErrorHandler) # retorna los errores como responses

d_base.metadata.create_all(
    bind=engine #parece que bind debe recibir el motor
    )

#------------ El modelo (clase) para la creacion de objetos "movies" --------

class JWTBearer(HTTPBearer):
    async def __call__(self, request: Request):
        auth = await super().__call__(request)
        data = validate_token(auth.credentials)
        if data["email"] != "admin@gmail.com":
            raise HTTPException(status_code= status.HTTP_401_UNAUTHORIZED, detail="Credenciales invalidas")

# Datos a modificar
movies= [
    {
        "id": 1,
        "title": "Avatar Ang",
        "overview": "En un exuberante planeta llamado Pandora viven los Na'vi, seres que ...",
        "year": "2009",
        "rating": 7.8,
        "category": "Comedia"
    },
    {
        "id": 2,
        "title": "Avatar",
        "overview": "En un exuberante planeta llamado Pandora viven los Na'vi, seres que ...",
        "year": "2009",
        "rating": 7.8,
        "category": "Acción"
    },
]

#------------ Get method --------
@user_router.get(path='/', tags=['home'])
#queremos que al entrar a la raiz de la web imprima un mensaje.
def message():
    return HTMLResponse('<h1>Hello word</h1>')

if __name__=="__main__":
    print(movies)            
        ## para activar un VENV usar en win .\venv\Scripts\activate  
        #Notas del curso: https://telegra.ph/Curso-de-FastAPI-Base-de-Datos-Modularizaci%C3%B3n-y-Deploy-a-Producci%C3%B3n-01-21