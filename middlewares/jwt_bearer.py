from fastapi import Request
from fastapi.security import HTTPBearer
from fastapi import HTTPException
from fastapi import status

from utils.jwt_manager import create_token
from utils.jwt_manager import validate_token


class JWTBearer(HTTPBearer):
    async def __call__(self, request: Request):
        auth = await super().__call__(request)
        data = validate_token(auth.credentials)
        if data["email"] != "admin@gmail.com":
            raise HTTPException(status_code= status.HTTP_401_UNAUTHORIZED, detail="Credenciales invalidas")
