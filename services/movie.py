from sqlalchemy.orm import Session
from models.movie import Movie as Movie_model
from schemas.movie import Movie
class MovieService():
    #db:Session=None

    def __init__(self,db:Session) -> None:
        """
            Cada vez que se llame este servicio, se le envie una sesion a la base de datos.

            db= sesion a la base de datos
        """
        self.db = db

    def get_movies(self):
        """
            Servicio traer todas las peliculas de la db
        """
        result = self.db.query(Movie_model).all() #query es para consultar, se le pasa el nombre de la tabla (el modelo)
        return result
    
    def get_movie_by_id(self,id): #aplica validaciones al Path parameter.
        """
            Filtra pelicula por el id en la db
        """
        result=self.db.query(Movie_model).filter(Movie_model.id == id).first()
        return result
    
    def get_movies_by_category(self, category):
        """
            Filtra pelicula por la categoria en la db
        """
        result=self.db.query(Movie_model).filter(Movie_model.category == category).all()
        return result

    def create_movie(self, movie:Movie):
        """
            Filtra pelicula por la categoria en la db
        """
        new_movie=Movie_model(**movie.dict()) #completamos la entidad db con el modelo dict
        self.db.add(new_movie)#agrega a la tabla
        self.db.commit()#guarda cambios en la tabla

    def update_movie(self, movie:Movie, id):
        """
            Actualiza una pelicula por por su ID en la db
        """
        result:Movie_model=self.db.query(Movie_model).filter(Movie_model.id == id).first()
        #Actualizar los siguientes datos del result
        result.title = movie.title
        result.overview = movie.overview
        result.year = movie.year
        result.rating = movie.rating
        result.category = movie.category
        self.db.commit() #guardar los cambios.

    def delete_movie(self, id):
        """
            Elimina una pelicula por por su ID en la db
        """
        result:Movie_model=self.db.query(Movie_model).filter(Movie_model.id == id).first()
        self.db.delete(result)
        self.db.commit() #guardar los cambios.

