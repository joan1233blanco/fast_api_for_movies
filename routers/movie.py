from fastapi import APIRouter
from fastapi import Body #se usa para que un conjunto de datos de entrada 
                            #se comporten como un request body y no como un query parameter
from fastapi import Path # permiten aplicar validaciones y configuracion a los tipos de parametros
from fastapi import Query # permiten aplicar validaciones y configuracion a los tipos de parametros
from fastapi import status # status code con alguna definicion
from fastapi.responses import JSONResponse # permite repuestas en formato Json
from fastapi.encoders import jsonable_encoder # encodear datos a Json

from typing import List # indica tipo de variable, puede contener otros tipos.

from config.database import session
from models.movie import Movie as Movie_dbmodel
from schemas.movie import Movie
from services.movie import MovieService

movie_router=APIRouter()
#queremos que cuando habra la direccion de Movies muestre la lista de peliculas
@movie_router.get(
    path='/movies', 
    tags=['Movies'], 
    status_code=status.HTTP_200_OK,
    response_model= List[Movie] # indica el tipo de repuesta
    #dependencies=[
    #    Depends(JWTBearer())
    #]
        ) #path decorator

def get_movies() -> List[Movie]:
    db = session()
    result=MovieService(db).get_movies()
    return JSONResponse( content=jsonable_encoder(result), status_code=status.HTTP_200_OK) # se especifica que es un formato Json

    #*********************** Path parameter *****************************

#queremos que al indicar el {id} en la direccion retorne la pelicula con el {id} indicado
#NOTA: por alguna razon solo funciona con id=1 //////// la identacion del return era erronea.
@movie_router.get(path='/movies/{id}', tags=['Movies'], response_model=Movie )
def get_movie_by_id(id: int = Path(ge=0, le=2000)) -> Movie: #aplica validaciones al Path parameter.
    db=session()
    result=MovieService(db).get_movie_by_id(id)
    if not result:
        return JSONResponse( content=[], status_code=status.HTTP_404_NOT_FOUND)
    return JSONResponse( content=jsonable_encoder(result), status_code=status.HTTP_200_OK)
    #********************************************************************

    #*********************** Query parameter *****************************

#queremos que por medio de el path /movies/ filtre las movies con query parameters
@movie_router.get(path='/movies/', tags=['Movies'], response_model=List[Movie])
def get_movies_by_category(category: str = Query(min_length=5, max_length=15)) -> List[Movie]:
    db=session()
    result=MovieService(db).get_movies_by_category(category)
    if not result:
        return JSONResponse( content=[], status_code=status.HTTP_404_NOT_FOUND)
    return JSONResponse( content=jsonable_encoder(result))
    #********************************************************************


#queremos que con el metodo post de cree una nueva pelucula
@movie_router.post(path='/movies', tags=['Movies'], response_model=dict,status_code=status.HTTP_201_CREATED)
def create_movie(movie:Movie)-> dict:
    db=session() #para conectar con la base de datos
    MovieService(db).create_movie(movie)
    return JSONResponse(status_code=status.HTTP_201_CREATED, content={"message": f"Se ha registrado la pelicula con el id {movie.id}"})

#------------ Put method --------

#con el metodo put se busca actualizar una movie en especifico
@movie_router.put('/movies/{id}', tags=['Movies'], response_model=dict, status_code=status.HTTP_201_CREATED)
def update_movie(id:int, movie:Movie) -> dict:
    db=session()
    try:
        MovieService(db).update_movie(movie,id)
        return JSONResponse( content={"message":f"Se actualizo la pelicula con el id : {id}"})
    except:
        return JSONResponse( content=[], status_code=status.HTTP_404_NOT_FOUND)

#------------ Delete method --------

#queremos que cuando se le pase un {id} al movies/{id} pero con el metodo delete, se borre la peli con ese id
@movie_router.delete('/movies/{id}', tags=['Movies'], response_model=dict, status_code=status.HTTP_202_ACCEPTED) # los tags organizan los metodos en la documentacion automatica
def delete_movie(id: int) -> dict:
    db=session()
    try:
        MovieService(db).delete_movie(id)
        return JSONResponse( content={"message":f"Se elimino la pelicula con el id : {id}"})
    except:
        return JSONResponse( content=[], status_code=status.HTTP_404_NOT_FOUND)