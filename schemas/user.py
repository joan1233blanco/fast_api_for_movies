from pydantic import BaseModel #permite la creacion facil de modelos

class User(BaseModel):
    '''
        Clase para el ojeto user, datos para autenticar.
    '''
    email:str
    pasword:str
